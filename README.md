Public Demo -> https://gitlab.com/Kryeger/3dot/-/releases

The main purpose of the player is to escape the building after hacking into the main terminal and extracting precious data. In their path, they will need to make use of the environment and his ability to save and reload at any time.

Enemies patrol the level, following an array of patrol points placed by hand. They can either loop around them or go back and forth, depending on   what was needed.
	They react intuitively to the player, stopping their patrol to chase them and only after losing sight returning to their way. Sound can also play a big part in their behaviour, as they can hear the player and will turn around if they do. This contributes to a tense and rewarding atmosphere, in which any mistakes can spell disaster and winning is a feat.
	If the player manages to escape the field of view of the enemy, the enemy will go to the last known position of the player, marked visually in the game as a white capsule.

Senses (vision and hearing) are implemented in a generic way, consisting of a series of raycasts around the entity that has them and a classification of the world in targets and obstacles. Targets will be gathered into an array while obstacles will block the raycast from reaching full length.
The sensor can be controlled using a maximum range (the maximum length a raycast can have) and the density of the raycasts. As such, a sensor will be more accurate if the length and the density are higher, but much more resource-intensive.
	To combat this issue, an optimization fix was implemented that allows sensor meshes to update only if they are a certain distance away from the player.

Saving and reloading have a somewhat less traditional meaning. Saving is a resource that has to be found and collected to be used. “Reloading is free” but a badly placed save point will almost certainly ruin the playthrough. If used correctly however, this mechanic can be very useful and provide entertaining ways of winning the level, whilst giving the player the option to test things in an otherwise punishing environment.

If the player is not careful and steps into the laser sensor, the alarm will go off. Along some visual changes, all enemies in the level will converge quickly on their location. This can be avoided by turning off the sensor using the laptop that controls the alarm.

To progress through the level, the player will need to find keycards that unlock matching doors. At the start of the game, the white keycard will be provided which unlocks almost all doors in the level, except a certain few. To open these, the player will need to explore the level, dodging and hiding away from the enemies.



Throughout the map the player can also find different objects with RigidBody attached to them, for some fun (and sometimes glitchy) Physics Interactions. There are also closets placed around the map. They serve both as places where they might find pick-ups (such as keycards or saves) or places where they can hide from the enemy. The player can open the closet when in its Trigger-collider. They close on themselves after a while. The closet doors also work with physics, having Hinge-Joints attached to them so that the player can easily spring outside of them at will.

The player can move at different speeds. The faster the noisier. Moving will emit noise. If the player makes noise in the enemy’s hearing range, they will get alerted and look towards the player. Going fast also depletes the player’s stamina, distorting their view.

In an attempt to make the experience smoother, tips are provided as part of the game world. This way, the much noisier text popups and hints are avoided, providing a better user experience.

To coordinate everything that didn’t make sense to include elsewhere, managers are used. They don’t play a major role, as most of the game results from the interactions of its systems, but they provide an easy way to manage things (UI and resources) that are not relevant to actual gameplay.

The game also features a minimalistic HUD. It helps the player keep track of their inventory and only pops when a new item is added or an owned item is used. Or if the player hits TAB. The Heads-up Display is made out of a Canvas with different Panels inside, each with Texts and Images. The icons are made by us.

The Pause Menu is built from a Canvas containing different Buttons. The Start Menu is built in a similar way, only this time we’ve made a separate Scene for it so we can load in and out of it when transitioning from the Game and the Start Menu.

For visual improvements we’ve used some PostFX scripts. We make use of Chromatic Aberration (to give feedback if the player is tired), Color Grading, Depth of Field (blurring the image when the Pause Menu is active), Motion Blur (makes running and transitioning to save-points look better), Film Grain, Ambient Occlusion (for darker corners), Bloom (makes emitter Materials glow) and Vignette.

All assets are made in house.

References:

The code on which the Sensors System is based is adapted from this series:
https://www.youtube.com/watch?v=rQG9aUWarwE
https://www.youtube.com/watch?v=73Dc5JTCmKI

The NavMesh AI is adapted from this tutorial:
https://www.youtube.com/watch?v=CHV1ymlw-P8

PostFX adapted from this tutorial:
https://www.youtube.com/watch?v=_PzYAbPpK8k

The 3D models are made by us, with great help from this tutorial:
https://www.youtube.com/watch?v=_HLMmaQM8Pg
From which we’ve also taken their Color Palette
https://www.dropbox.com/s/c5olic38j8fopet/ImphenziaPalette01.png?dl=0
