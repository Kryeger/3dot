﻿using System;
using System.Collections.Generic;
using Scripts.Manager;
using UnityEngine;

namespace Scripts {
    public class AlarmController : MonoBehaviour {
        private GameManager _gameManager;

        public bool alarmStatus = false;
        public bool disabled = false;

        public Material screenMat;
        public Color defaultScreenColor;

        public List<GameObject> alarmObjects = new List<GameObject>();
        // Start is called before the first frame update]

        private void Awake() {
            _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

            _gameManager.alarmEvent.AddListener(() => {
                alarmStatus = true;
                screenMat.SetColor("_EmissionColor", Color.red);
            });
            _gameManager.stopAlarmEvent.AddListener(() => {
                Debug.Log("Stopped alarm");
                alarmStatus = false;
                screenMat.SetColor("_EmissionColor", defaultScreenColor);
                disabled = true;
                alarmObjects.ForEach((obj) => {
                    obj.SetActive(false);
                });
            });

            screenMat = Utils.GetChildByName(gameObject, "Screen").GetComponent<Renderer>().material;
            defaultScreenColor = screenMat.GetColor("_EmissionColor");
        }

        private void OnTriggerStay(Collider other) {
            if (other.gameObject.CompareTag("Player")) {
                if (Input.GetKeyDown(KeyCode.E) && !disabled) {
                    _gameManager.stopAlarmEvent.Invoke();
                }
            }
        }
    }
}
