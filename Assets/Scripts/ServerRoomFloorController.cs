﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerRoomFloorController : MonoBehaviour
{
    // Start is called before the first frame update
    public ServerLaptopController serverLaptopController;
    void Start()
    {
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartDownload()
    {
        gameObject.SetActive(true);
    }

    public void StopDownload()
    {
        gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !serverLaptopController.IsDownloadFinished())
        {
            serverLaptopController.downloadIsActive = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player") && !serverLaptopController.IsDownloadFinished())
        {
            serverLaptopController.downloadIsActive = false;
        }
    }
}
