﻿using System;
using Scripts.Manager;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace Scripts {
    public class PlayerController : MonoBehaviour {
        private float movementSpeed = Utils.GetSpeed(Speed.PlayerWalking);
        public float rotationSpeed = 10f;
        [Range(0, 1)]
        public float movementLerp = .2f;
        [Range(0, 1)]
        public float stoppingLerp = .7f;

        private Vector3 _movementInput;

        public Camera viewCamera;
        public Vector3 velocity;

        private Vector3 _mousePosition;
        private Rigidbody _rigidbody;

        private bool _isRunning = false;
        private bool _isCrouching = false;

        public bool isCrouching
        {
            get => _isCrouching;
        }

        public PostFxController postFxController;

        public CameraController cameraController;
        public FovController fovController;
        public StaminaController staminaController;
        public InventoryController inventoryController;
        public SaveController saveController;
        public HudManager hudManager;
        public GameManager gameManager;

        public GameObject playerSpotlight;
        private Vector3 _playerSpotlightLocalOffset = new Vector3(0f, 7.2f, 0f);
        private Vector3 _playerSpotlightCrouchingLocalOffset;

        private Vector3 _initialLocalScale;
        private Vector3 _crouchModifiers = new Vector3(1f, .7f, 1f);
        private Vector3 _crouchLocalScale;

        private Vector3 _initialFovOffset;
        private Vector3 _crouchFovOffset;

        public NoiseManager noiseManager;

        public GameObject bombPrefab;

        public int deathCount;

        private GameObject debugLight;

        // Start is called before the first frame update
        private void Awake() {
            _rigidbody = GetComponent<Rigidbody>();
            viewCamera = Camera.main;

            cameraController = viewCamera.GetComponent<CameraController>();

            staminaController = GetComponent<StaminaController>();
            fovController = GetComponent<FovController>();
            inventoryController = GetComponent<InventoryController>();
            saveController = GetComponent<SaveController>();

            hudManager = GameObject.Find("HudManager").GetComponent<HudManager>();
            noiseManager = GameObject.Find("NoiseManager").GetComponent<NoiseManager>();
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

            postFxController = GameObject.FindWithTag("PostFX").GetComponent<PostFxController>();

            debugLight = GameObject.Find("Debug Light");
            if(debugLight != null) {
                debugLight.SetActive(false);
            }

            playerSpotlight.transform.localPosition = _playerSpotlightLocalOffset;
            var localScale = transform.localScale;
            _initialLocalScale = new Vector3(localScale.x, localScale.y, localScale.z);
            _crouchLocalScale = Vector3.Scale(_initialLocalScale, _crouchModifiers);
            _playerSpotlightCrouchingLocalOffset = new Vector3(
                _playerSpotlightLocalOffset.x,
                _playerSpotlightLocalOffset.y + (_initialLocalScale.y - _crouchLocalScale.y) * 2.5f,
                _playerSpotlightLocalOffset.z);
            _initialFovOffset = fovController.fovPositionOffset;
            _crouchFovOffset = Vector3.Scale(_initialFovOffset, _crouchModifiers);
        }

        // Update is called once per frame
        private void Update() {
            HandleMovementInput();
            HandleInput();
            HandleCrouching();
            Rotate();
            HandleFx();
        }

        private void FixedUpdate() {
            ExecuteMovement();
        }

        private void OnTriggerEnter(Collider other) {
            switch (other.tag) {
                case "Item":
                    switch (other.GetComponent<ItemController>().type) {
                        case ItemTypes.Save:

                            inventoryController.saveCount++;
                            break;

                        case ItemTypes.WhiteKey:
                            inventoryController.whiteKeyStatus = true;
                            break;

                        case ItemTypes.RedKey:
                            inventoryController.redKeyStatus = true;
                            break;

                        case ItemTypes.GreenKey:
                            inventoryController.greenKeyStatus = true;
                            break;

                        case ItemTypes.BlueKey:
                            inventoryController.blueKeyStatus = true;
                            break;
                    }
                    other.gameObject.SetActive(false);
                    break;

                case "EndZone":
                    if (inventoryController.hasTarget)
                    {
                        gameManager.ShowEndScreen(true);
                    }
                    break;
            }
        }

        private void HandleFx() {
            // TODO: find better value transformation
            postFxController.chrAbbIntensity = (100 - staminaController.stamina < 50) ? ((100 - staminaController.stamina) / 100) - .5f : (100 - staminaController.stamina) / 100;
        }

        private void HandleMovementInput()
        {

            _movementInput = (gameManager.endMessage) ? Vector3.zero : new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
            if (_movementInput.magnitude > 0)
            {
                velocity = Vector3.Lerp(velocity, _movementInput.normalized *
                                                  ProcessMovementSpeed(), movementLerp);
            }
            else
            {
                velocity = Vector3.Lerp(velocity, _movementInput.normalized *
                                                  ProcessMovementSpeed(), stoppingLerp);
            }
        }

        private float ProcessMovementSpeed() {
            var speed = movementSpeed;

            if (_isCrouching && _movementInput.magnitude > 0)
            {
                speed = Utils.GetSpeed(Speed.PlayerCrouching);
            }

            if (_isRunning && _movementInput.magnitude > 0) {
                if (staminaController.StartConsuming()) {
                    speed = Utils.GetSpeed(Speed.PlayerRunning);
                }
            }
            else {
                staminaController.StopConsuming();
            }

            return speed;
        }

        private void HandleInput()
        {
            if (hudManager.isPaused || gameManager.endMessage) return;

            if (Math.Abs(Input.GetAxis("Mouse ScrollWheel")) > 0.0001f) // forward
            {
                viewCamera.orthographicSize -= Input.GetAxis("Mouse ScrollWheel") * 5;
            }

            if (Input.GetKeyDown(KeyCode.LeftControl)) {
                _isCrouching = true;
                _isRunning = false;
            }

            if (Input.GetKeyUp(KeyCode.LeftControl))
            {
                _isCrouching = false;
            }

            if (Input.GetKeyDown(KeyCode.LeftShift)) {
                _isRunning = true;
                _isCrouching = false;
            }

            if (Input.GetKeyUp(KeyCode.LeftShift)) {
                _isRunning = false;
            }

            if (Input.GetKeyDown(KeyCode.F)) {
                if (inventoryController.bombCount > 0) {
                    Instantiate(bombPrefab, transform.position, Quaternion.Euler(new Vector3(0, transform.eulerAngles.y, 90)));
                    inventoryController.bombCount--;
                }
            }

            if (Input.GetKeyDown(KeyCode.C)) {
                if (inventoryController.saveCount > 0) {
                    inventoryController.saveCount--;
                    saveController.Save();
                }
            }

            if (Input.GetKeyDown(KeyCode.R)) {
                var saveCount = inventoryController.saveCount;
                saveController.Load();
                inventoryController.saveCount = saveCount;
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                inventoryController.ShowInventoryUI();
            }

            // debug cheats
            if (Input.GetKeyDown(KeyCode.Keypad0)) {
                debugLight.SetActive(!debugLight.activeSelf);
            }
            if (Input.GetKeyDown(KeyCode.Keypad1))
            {
                inventoryController.whiteKeyStatus = !inventoryController.whiteKeyStatus;
            }
            if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                inventoryController.redKeyStatus = !inventoryController.redKeyStatus;
            }
            if (Input.GetKeyDown(KeyCode.Keypad3))
            {
                inventoryController.greenKeyStatus = !inventoryController.greenKeyStatus;
            }
            if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                inventoryController.blueKeyStatus = !inventoryController.blueKeyStatus;
            }
            if (Input.GetKeyDown(KeyCode.Keypad5))
            {
                inventoryController.hasTarget = !inventoryController.hasTarget;
            }
            if (Input.GetKeyDown(KeyCode.KeypadMultiply))
            {
                gameManager.godMode = !gameManager.godMode;
            }
            if (Input.GetKeyDown(KeyCode.KeypadPlus)) {
                inventoryController.saveCount += 1;
                inventoryController.bombCount += 1;
            }
        }

        private void Rotate() {
            _mousePosition = viewCamera.ScreenToWorldPoint(
                new Vector3(Input.mousePosition.x, Input.mousePosition.y, viewCamera.transform.position.y)
            );

            Quaternion targetRotation = Quaternion.LookRotation(_mousePosition - transform.position);
            targetRotation.x = 0;
            targetRotation.z = 0;

            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }

        private void ExecuteMovement()
        {
            var position = _rigidbody.position;

            var oldVelocity = new Vector3(velocity.x, velocity.y, velocity.z);
            velocity *= (Time.fixedDeltaTime * 60);

            _rigidbody.velocity = new Vector3(velocity.x, _rigidbody.velocity.y, velocity.z);

            _rigidbody.position = new Vector3(_rigidbody.position.x, Mathf.Clamp(position.y, 0, 3f), _rigidbody.position.z);

            if (velocity.magnitude > 0)
            {
                noiseManager.MakeNoise(gameObject.GetInstanceID(), transform.position, oldVelocity.magnitude / 2);
            }

        }

        private GameObject GetChildByName(string childName) {
            return Utils.GetChildByName(gameObject, childName);
        }

        private void HandleCrouching()
        {
            float lerpT = .5f;
            if (_isCrouching)
            {
                transform.localScale = Vector3.Lerp(transform.localScale, _crouchLocalScale, lerpT);
                transform.localPosition =
                    transform.localPosition - (transform.localScale - _crouchLocalScale);
                playerSpotlight.transform.localPosition = Vector3.Lerp(playerSpotlight.transform.localPosition,
                    _playerSpotlightCrouchingLocalOffset, lerpT);
                fovController.fovPositionOffset =
                    Vector3.Lerp(fovController.fovPositionOffset, _crouchFovOffset, lerpT);
            }
            else
            {
                transform.localScale = Vector3.Lerp(transform.localScale, _initialLocalScale, lerpT);
                transform.localPosition =
                    transform.localPosition + (_initialLocalScale - transform.localScale);
                playerSpotlight.transform.localPosition = Vector3.Lerp(playerSpotlight.transform.localPosition,
                    _playerSpotlightLocalOffset, lerpT);
                fovController.fovPositionOffset =
                    Vector3.Lerp(fovController.fovPositionOffset, _initialFovOffset, lerpT);
            }
        }

    }
}