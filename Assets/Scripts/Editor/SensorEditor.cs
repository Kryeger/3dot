﻿using UnityEditor;
using UnityEngine;
using Scripts;

namespace Editor {

    [CustomEditor(typeof(SensorController))]
    public class SensorEditor : UnityEditor.Editor
    {
        private void OnSceneGUI()
        {
            var sensorController = (SensorController) target;
            var sensorTransformPosition = sensorController.transform.position;

            Handles.color = Color.white;
            Handles.DrawWireArc(
                sensorTransformPosition,
                Vector3.up,
                Vector3.forward,
                360,
                sensorController.sensorRange);

            var viewAngleA = sensorController.DirFromAngle(-sensorController.focusAngle / 2);
            var viewAngleB = sensorController.DirFromAngle(sensorController.focusAngle / 2);

            Handles.DrawLine(sensorTransformPosition, sensorTransformPosition + viewAngleA * sensorController.sensorRange);
            Handles.DrawLine(sensorTransformPosition, sensorTransformPosition + viewAngleB * sensorController.sensorRange);

            Handles.color = Color.red;
            foreach (var targetInFocus in sensorController.targetsInFocus)
            {
                Handles.DrawLine(sensorController.transform.position, targetInFocus);
            }

        }
    }
}
