﻿using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace Scripts {

    public class NoiseController : MonoBehaviour {
        public float noiseRadius {
            get => transform.localScale.x;
            set => transform.localScale = new Vector3(value, .1f, value);
        }

        private Coroutine _expireCoroutine;

        public float fadeTimeout = .1f;

        // Start is called before the first frame update
        void Start() {
            gameObject.SetActive(false);
        }

        public void SpawnAt(Vector3 position, float intensity) {
            gameObject.transform.position = position;
            noiseRadius = Mathf.Lerp(noiseRadius, intensity, .3f);
            gameObject.SetActive(true);

            if(_expireCoroutine != null) StopCoroutine(_expireCoroutine);
            _expireCoroutine = StartCoroutine(Expire());
        }

        private IEnumerator Expire() {
            yield return new WaitForSeconds(fadeTimeout);
            gameObject.SetActive(false);
        }
    }
}