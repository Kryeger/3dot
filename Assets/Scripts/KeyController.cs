﻿using System;
using System.Collections.Generic;
using Scripts.Manager;
using UnityEngine;

namespace Scripts {
    public class KeyController : ItemController {

        public Renderer _renderer;
        public ResManager resManager;

        public KeyController() {
            type = ItemTypes.WhiteKey;
        }

        private void Awake()
        {
            resManager = GameObject.Find("ResManager").GetComponent<ResManager>();
        }

        // Start is called before the first frame update
        private void Start() {
            UpdateMaterial();
        }

        #if UNITY_EDITOR
        private void OnValidate() {
            if (_renderer == null) return;
            _renderer.material = ResManagerEditor.KeyMaterials[type];
        }
        #endif

        // Update is called once per frame
        void Update()
        {
        
        }

        void UpdateMaterial() {
            if (_renderer == null) return;
            _renderer.material = resManager.KeyMaterials[type];
        }

    }
}
