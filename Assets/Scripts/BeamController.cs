﻿using System;
using Scripts.Manager;
using UnityEngine;

namespace Scripts {
    public class BeamController : MonoBehaviour
    {
        private GameManager gameManager;


        private void Start() {
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        }
        private void OnTriggerEnter(Collider other) {
            if (other.CompareTag("Player")) {
                gameManager.alarmEvent.Invoke();
            }
        }
    }
}
