﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VendingController : MonoBehaviour
{
    public Rigidbody sodaCanRb;

    private float _forceMultiplier = 5f;

    private bool _isEmpty = false;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (!_isEmpty)
                {
                    sodaCanRb.isKinematic = false;
                    sodaCanRb.AddRelativeForce(Vector3.forward * -_forceMultiplier, ForceMode.Impulse);
                    _isEmpty = true;
                }
            }
        }
    }
}
