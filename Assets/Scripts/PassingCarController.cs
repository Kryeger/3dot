﻿using System;
using System.Collections;
using System.Collections.Generic;
using Scripts;
using UnityEngine;

public class PassingCarController : MonoBehaviour
{
    public float start = -174f;
    public float speed = 30f;

    public float finish = 157f;
    private float _direction = 1f;

    public bool horizontal;

    private void Start()
    {
        ResetToStart();
        if (finish > start)
        {
            _direction = 1;
        }
        else
        {
            _direction = -1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * (speed * Time.deltaTime));

        if (horizontal)
        {
            if (transform.position.x >= finish && _direction > 0)
            {
                ResetToStart();
            }

            if (transform.position.x <= finish && _direction < 0)
            {
                ResetToStart();
            }

        }
        else
        {
            if (transform.position.z >= finish && _direction > 0)
            {
                ResetToStart();
            }

            if (transform.position.z <= finish && _direction < 0)
            {
                ResetToStart();
            }
        }
    }

    private void ResetToStart()
    {
        var oldPosition = transform.position;
        if (horizontal)
        {
            oldPosition.x = start;
        }
        else
        {
            oldPosition.z = start;
        }
        transform.position = oldPosition;
    }
}
