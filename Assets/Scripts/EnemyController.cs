﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Scripts.Manager;
using UnityEngine;
using UnityEngine.AI;

namespace Scripts {
    public enum EnemyState {
        Stopped,
        StartingPatrol,
        Patroling,
        ChasingPlayer,
        ChasingGhost,
        HeardNoise
    }

    public enum LookState {
        Forward,
        SideToSide,
        ToTarget
    }

    public class EnemyController : MonoBehaviour {
        public GameObject model;

        public NavMeshAgent agent;

        public GameObject player;

        public GameObject patrol;
        public PatrolController patrolController;

        public FovController fovController;

        public HearingController hearingController;

        public StaminaController staminaController;

        public HudManager hudManager;
        public GameManager gameManager;

        public GameObject ghostPrefab;
        private GameObject _playerGhostObject;

        private float _ghostExpirationTimeout;
        private Coroutine _ghostExpirationCoroutine;

        private float _minDistanceFromPlayer = 2f;

        private Vector3 _lookTarget;

        public float rotationSpeed = Utils.GetRotationSpeed(RotationSpeed.Normal);

        private Vector3 lookTarget {
            get => _lookTarget;
            set => _lookTarget = new Vector3(value.x, model.transform.position.y, value.z);
        }

        private LookState _lookState;

        public LookState lookState {
            get => _lookState;
            set {

                switch (value) {
                    case LookState.Forward:
                        break;
                    case LookState.ToTarget:
                        break;
                    case LookState.SideToSide:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(value), value, null);
                }

                _lookState = value;
            }
        }

        private Vector3 moveTarget { get; set; }

        private Speed _movementState;

        public Speed movementState {
            get => _movementState;

            set {
                switch (value) {
                    case Speed.Running:
                        break;
                    case Speed.Walking:
                        break;
                    case Speed.Stopped:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(value), value, null);
                }

                _movementState = value;
            }
        }

        private Vector3 _noiseTarget;

        public float speed {
            get => agent.speed;
            set => agent.speed = value;
        }

        private EnemyState _currentState;
        private EnemyState _previousState;
        public EnemyState startingState = EnemyState.StartingPatrol;

        private EnemyState currentState {
            get => _currentState;

            set {
                switch (value) {
                    case EnemyState.Stopped:
                        break;
                    case EnemyState.StartingPatrol:
                        patrolController.AdvanceToClosestPointIndex(model.transform.position);

                        movementState = Speed.Walking;
                        lookState = LookState.Forward;

                        rotationSpeed = Utils.GetRotationSpeed(RotationSpeed.Slow);
                        break;
                    case EnemyState.Patroling:

                        movementState = Speed.Walking;
                        lookState = LookState.Forward;

                        var ct = patrolController.currentTarget;
                        if (ct != null)
                        {
                            UpdateDestination(patrolController.currentTarget.transform.position);
                        }
                        break;
                    case EnemyState.ChasingPlayer:
                        DestroyGhost();
                        movementState = Speed.Running;
                        lookState = LookState.Forward;

                        rotationSpeed = Utils.GetRotationSpeed(RotationSpeed.Normal);
                        UpdateDestination(player.transform.position);
                        break;
                    case EnemyState.ChasingGhost:
                        SpawnGhost();
                        movementState = Speed.Running;
                        lookState = LookState.Forward;
                        break;
                    case EnemyState.HeardNoise:
                        lookTarget = _noiseTarget;
                        lookState = LookState.ToTarget;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(value), value, null);
                }

                if (_currentState != EnemyState.HeardNoise)
                {
                    _previousState = _currentState;
                }

                _currentState = value;
            }
        }

        private GameManager _gameManager;

        private void Start() {
            model = GetChildByName("Model");

            patrol = GetChildByName("Patrol");
            patrolController = patrol.GetComponent<PatrolController>();

            fovController = model.GetComponent<FovController>();
            staminaController = GetComponent<StaminaController>();
            hearingController = model.GetComponent<HearingController>();

            hudManager = GameObject.Find("HudManager").GetComponent<HudManager>();
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

            agent = model.GetComponent<NavMeshAgent>();
            agent.updateRotation = false;

            speed = Utils.GetSpeed(Speed.Walking);

            player = GameObject.FindWithTag("Player");

            _lookTarget = new Vector3(0, transform.eulerAngles.y, 0);
            _currentState = EnemyState.StartingPatrol;

            _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

            _gameManager.alarmEvent.AddListener(OnAlarmEvent);

            _playerGhostObject = Instantiate(ghostPrefab, new Vector3(0, -100, 0), Quaternion.identity);
            _playerGhostObject.SetActive(false);

            currentState = startingState;
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.white;
            if ((player != null) && GhostIsPlayer()) {
                Gizmos.color = Color.red;
            }

            Gizmos.DrawSphere(moveTarget, .5f);
        }

        private void Update()
        {
            if (gameManager.endMessage)
            {
                agent.enabled = false;
                return;
            }

            agent.enabled = true;

            UpdateRotation();
            HandleGui();

            if (CanSeePlayer()) {
                if (Vector3.Distance(
                        new Vector3(model.transform.position.x,0, model.transform.position.z),
                        new Vector3(player.transform.position.x,0, player.transform.position.z)
                    ) <= _minDistanceFromPlayer)
                {
                    gameManager.PlayerGotCaught();
                }
                currentState = EnemyState.ChasingPlayer;
            }

            if (currentState != EnemyState.ChasingPlayer && HeardSomething())
            {
                currentState = EnemyState.HeardNoise;
            }

            switch (currentState) {
                case EnemyState.Stopped:
                    break;
                case EnemyState.StartingPatrol:
                    currentState = EnemyState.Patroling;
                    break;
                case EnemyState.Patroling:
                    if (ArrivedAtMoveTarget()) {
                        patrolController.AdvanceTargetIndex();
                        currentState = EnemyState.Patroling;
                    }

                    break;
                case EnemyState.ChasingPlayer:
                    if (!CanSeePlayer()) {
                        currentState = EnemyState.ChasingGhost;
                    }

                    break;
                case EnemyState.ChasingGhost:
                    if (ArrivedAtMoveTarget()) {
                        DestroyGhost();
                        currentState = EnemyState.StartingPatrol;
                    }

                    break;
                case EnemyState.HeardNoise :
                    if (Vector3.Angle(model.transform.forward, lookTarget - model.transform.position) < 1f)
                    {
                        currentState = _previousState;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            switch (movementState) {
                case Speed.Running:
                    if (staminaController.StartConsuming()) {
                        speed = Utils.GetSpeed(Speed.Running);
                    } else {
                        speed = Utils.GetSpeed(Speed.Walking);
                    }

                    break;
                case Speed.Walking:
                    staminaController.StopConsuming();
                    speed = Utils.GetSpeed(Speed.Walking);
                    break;
                case Speed.Stopped:
                    staminaController.StopConsuming();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }

        private void HandleGui() {
        }

        private bool GhostIsPlayer() {
            return Utils.IsClose(moveTarget, player.transform.position);
        }

        private bool CanSeePlayer() {
            return fovController.targetsInFocus.Count > 0 && !gameManager.godMode;
        }

        private bool HeardSomething()
        {
            if (hearingController.targetsInFocus.Count > 0)
            {
                _noiseTarget = hearingController.targetsInFocus[0];
            }
            return hearingController.targetsInFocus.Count > 0;
        }

        private bool ArrivedAtMoveTarget() {
            var position = model.transform.position;

            var x = new Vector3(position.x, 0, position.z);
            var y = new Vector3(moveTarget.x, 0, moveTarget.z);

            return Utils.IsClose(x, y);
        }

        void UpdateDestination(Vector3 destination) {
            moveTarget = destination;
            lookTarget = moveTarget;
            agent.SetDestination(moveTarget);
        }

        private void UpdateRotation() {

            var oldRotation = model.transform.rotation;
            switch(_lookState) {
                case LookState.Forward:
                    if (agent.velocity.normalized != Vector3.zero) {
                        model.transform.rotation =
                            Quaternion.Lerp(oldRotation, Quaternion.LookRotation(agent.velocity.normalized),  rotationSpeed * Time.deltaTime);
                    }
                    break;
                case LookState.ToTarget:
                    model.transform.LookAt(lookTarget);
                    model.transform.rotation =
                        Quaternion.Lerp(oldRotation, model.transform.rotation, rotationSpeed * Time.deltaTime);
                    break;
                case LookState.SideToSide:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private GameObject GetChildByName(string childName) {
            return Utils.GetChildByName(gameObject, childName);
        }

        private void SpawnGhost(Vector3 location)
        {
            _playerGhostObject.SetActive(true);
            _playerGhostObject.transform.position = location;
        }
        private void SpawnGhost()
        {
            SpawnGhost(moveTarget);
        }

        private void DestroyGhost()
        {
            _playerGhostObject.SetActive(false);
        }

        private void OnAlarmEvent() {
            UpdateDestination(player.transform.position);
            currentState = EnemyState.ChasingGhost;
            SpawnGhost();
        }

        public void HearBombExplosion(Vector3 location) {
            UpdateDestination(location);
            currentState = EnemyState.ChasingGhost;
            SpawnGhost();
        }

    }
}