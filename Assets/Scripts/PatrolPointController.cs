﻿using System;
using UnityEngine;

namespace Scripts {
    public class PatrolPointController : MonoBehaviour {
        private void OnDrawGizmos() {
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(transform.position, .2f);
        }
    }
}