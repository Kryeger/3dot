﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

namespace Scripts {
    public class FovController : SensorController
    {
        public Vector3 fovPositionOffset = new Vector3(0, 1.5f, 0);
        public Vector3 fovTargetPositionOffset = new Vector3(0, 0, 0);

        protected override Vector3 GetPositionOffset()
        {
            return fovPositionOffset;
        }
        protected override Vector3 GetTargetPositionOffset()
        {
            return fovTargetPositionOffset;
        }
    }
}