﻿using System.Collections;
using Scripts.Manager;
using UnityEngine;

namespace Scripts {
    public class BombController : MonoBehaviour {
        public float timeToExplosion = 2f;
        public float maxRadius = 25f;

        private SphereCollider _collider;
        private GameObject _soundRadius;

        private GameManager _gameManager;

        void Start() {
            _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            _collider = GetComponent<SphereCollider>();
            _soundRadius = Utils.GetChildByName(gameObject, "SoundRadius");

            StartCoroutine(Explode());
        }

        private IEnumerator Explode() {
            yield return new WaitForSeconds(timeToExplosion);
            while (_collider.radius < maxRadius) {
                _soundRadius.transform.localScale += new Vector3(0, .8f, .8f);
                _collider.radius += .4f;
                yield return new WaitForSeconds(.01f);
            }
            Destroy(this.gameObject);
        }

        private void OnTriggerEnter(Collider other) {
            if(other.CompareTag("EnemyModel")){
                // Debug.Log("Found enemy at" + other.transform.position);
                other.gameObject.GetComponentInParent<EnemyController>().HearBombExplosion(transform.position);
            }
        }
    }
}
