﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scripts.Manager;
using UnityEngine;

namespace Scripts {

    public class SaveController : MonoBehaviour {
        public Save save;

        private GameObject _player;
        private PlayerController _playerController;
        private AlarmController _alarmController;
        private GameManager _gameManager;

        private List<KeyController> _keys;

        private void Awake()
        {
            _player = GameObject.Find("Player");
            _playerController = _player.GetComponent<PlayerController>();
        }

        private void Start() {
            _alarmController = GameObject.Find("AlarmLaptop").GetComponent<AlarmController>();
            _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            _keys = FindObjectsOfType<KeyController>().ToList();


            if (SceneComm.ShouldLoad) {
                LoadFromSave(SceneComm.Save);
            }
        }



        public void Save() {
            save = new Save(_playerController, _alarmController);
            SaveToFile();
        }

        public void SaveToFile() {
            if (save == null) return;
            System.IO.File.WriteAllText(Application.persistentDataPath + "/save.json", JsonUtility.ToJson(save));
        }

        public void LoadFromFile() {
            var foundSave = JsonUtility.FromJson<Save>(System.IO.File.ReadAllText(Application.persistentDataPath + "/save.json"));
            if (foundSave != null) {
                save = foundSave;
            }

        }

        public void LoadFromSave(Save save) {
            if (save == null) return;
            this.save = save;
            _player.transform.position = save.Position;

            _playerController.inventoryController.LoadFromSave(save.Inventory);

            _keys.ForEach(key => {
                switch (key.type) {
                    case ItemTypes.RedKey:
                        key.gameObject.SetActive(!save.Inventory.redKeyStatus);
                        break;
                    case ItemTypes.GreenKey:
                        key.gameObject.SetActive(!save.Inventory.greenKeyStatus);
                        break;
                    case ItemTypes.BlueKey:
                        key.gameObject.SetActive(!save.Inventory.blueKeyStatus);
                        break;
                    case ItemTypes.WhiteKey:
                        key.gameObject.SetActive(!save.Inventory.whiteKeyStatus);
                        break;
                    default:
                        Debug.Log("That's not a Key!");
                        throw new ArgumentOutOfRangeException();
                }
            });

            if (save.alarmStatus && !save.alarmDisabled) {
                _gameManager.alarmEvent.Invoke();
            } else {
                _gameManager.stopAlarmEvent.Invoke();
            }
        }

        public void Load() {
            if (save == null) return;
            _player.transform.position = save.Position;
            _playerController.inventoryController.LoadFromSave(save.Inventory);

            _keys.ForEach(key => {
                switch (key.type) {
                    case ItemTypes.RedKey:
                        key.gameObject.SetActive(!save.Inventory.redKeyStatus);
                        break;
                    case ItemTypes.GreenKey:
                        key.gameObject.SetActive(!save.Inventory.greenKeyStatus);
                        break;
                    case ItemTypes.BlueKey:
                        key.gameObject.SetActive(!save.Inventory.blueKeyStatus);
                        break;
                    case ItemTypes.WhiteKey:
                        key.gameObject.SetActive(!save.Inventory.whiteKeyStatus);
                        break;
                    default:
                        Debug.Log("That's not a Key!");
                        throw new ArgumentOutOfRangeException();
                }
            });

            if (save.alarmStatus && !save.alarmDisabled) {
                _gameManager.alarmEvent.Invoke();
            } else {
                _gameManager.stopAlarmEvent.Invoke();
            }
        }
    }


}
