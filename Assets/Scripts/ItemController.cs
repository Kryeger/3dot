﻿using UnityEngine;

namespace Scripts {

    public enum ItemTypes {
        Save,
        RedKey,
        GreenKey,
        BlueKey,
        WhiteKey,
        NoItem
    }
    public class ItemController : MonoBehaviour {

        public ItemTypes type = ItemTypes.NoItem;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
