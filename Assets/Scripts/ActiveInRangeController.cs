﻿using System;
using System.Collections;
using System.Collections.Generic;
using Scripts;
using UnityEngine;

public class ActiveInRangeController : MonoBehaviour
{
    private float _initialAlpha = 1f;

    private TextMesh _textMesh;
    private PlayerController _playerController;

    public bool ifWhiteKeyIsMissing, ifRedKeyIsMissing, ifGreenKeyIsMissing, ifBlueKeyIsMissing;

    // Start is called before the first frame update
    void Start()
    {
        _textMesh = GetComponent<TextMesh>();
        _initialAlpha = _textMesh.color.a;
        _playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        SetColorAlpha(0);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (ShouldShow())
            {
                SetColorAlpha(_initialAlpha);
            }
            else
            {
                gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // SetColorAlpha(0);
        }
    }

    private void SetColorAlpha (float newAlpha)
    {
        var oldColor = _textMesh.color;
        _textMesh.color = new Color(oldColor.r, oldColor.g, oldColor.b, newAlpha);
    }

    private bool ShouldShow()
    {
        var inventory = _playerController.inventoryController;
        return (!inventory.whiteKeyStatus && ifWhiteKeyIsMissing) ||
               (!inventory.redKeyStatus && ifRedKeyIsMissing) ||
               (!inventory.greenKeyStatus && ifGreenKeyIsMissing) ||
               (!inventory.blueKeyStatus && ifBlueKeyIsMissing) ||
            (!ifWhiteKeyIsMissing && !ifRedKeyIsMissing && !ifGreenKeyIsMissing && !ifBlueKeyIsMissing);
    }
}
