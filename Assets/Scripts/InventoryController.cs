﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts {
    public class InventoryController : MonoBehaviour
    {
        public void LoadFromSave (InventorySave inventorySave) {
            saveCount = inventorySave.saveCount;
            redKeyStatus = inventorySave.redKeyStatus;
            greenKeyStatus = inventorySave.greenKeyStatus;
            whiteKeyStatus = inventorySave.whiteKeyStatus;
            blueKeyStatus = inventorySave.blueKeyStatus;
            hasTarget = inventorySave.hasTarget;
            bombCount = inventorySave.bombCount;
        }

        private int _saveCount = 1;
        private int _bombCount = 3;
        private bool _redKeyStatus, _blueKeyStatus, _greenKeyStatus, _whiteKeyStatus, _hasTarget;

        public bool redKeyStatus
        {
            get => _redKeyStatus;
            set
            {
                RedKeyImage.color = value ? _redKeyImageColor : _disabledKeyImageColor;
                ShowInventoryUI();
                _redKeyStatus = value;
            }
        }
        public bool blueKeyStatus
        {
            get => _blueKeyStatus;
            set
            {
                BlueKeyImage.color = value ? _blueKeyImageColor : _disabledKeyImageColor;
                ShowInventoryUI();
                _blueKeyStatus = value;
            }
        }
        public bool greenKeyStatus
        {
            get => _greenKeyStatus;
            set
            {
                GreenKeyImage.color = value ? _greenKeyImageColor : _disabledKeyImageColor;
                ShowInventoryUI();
                _greenKeyStatus = value;
            }
        }
        public bool whiteKeyStatus
        {
            get => _whiteKeyStatus;
            set
            {
                WhiteKeyImage.color = value ? _whiteKeyImageColor : _disabledKeyImageColor;
                ShowInventoryUI();
                _whiteKeyStatus = value;
            }
        }
        public bool hasTarget
        {
            get => _hasTarget;
            set
            {
                TargetPanelUI.SetActive(value);
                _hasTarget = value;
            }
        }

        public int saveCount
        {
            get => _saveCount;
            set
            {
                SaveCountUIText.text = $"{value:00}";
                ShowInventoryUI();
                _saveCount = value;
            }
        }

        public int bombCount
        {
            get => _bombCount;
            set
            {
                BombCountUIText.text = $"{value:00}";
                ShowInventoryUI();
                _bombCount = value;
            }
        }

        public GameObject InventoryUIGroup;
        public Text SaveCountUIText;
        public Text BombCountUIText;

        public Image WhiteKeyImage;
        public Image RedKeyImage;
        public Image GreenKeyImage;
        public Image BlueKeyImage;

        public GameObject TargetPanelUI;

        private Color _disabledKeyImageColor = new Color(1f, 1f, 1, .5f);
        private Color _whiteKeyImageColor = Color.white;
        private Color _redKeyImageColor = new Color(243f/255f, 66f/255f, 66f/255f);
        private Color _greenKeyImageColor = new Color(94f/255f, 212f/255f, 75f/255f);
        private Color _blueKeyImageColor = new Color(98f/255f, 135f/255f, 1f);

        private void Start()
        {
            InventoryUIGroup.SetActive(false);
            SaveCountUIText.text = $"{saveCount:00}";
            BombCountUIText.text = $"{bombCount:00}";


            WhiteKeyImage.color = whiteKeyStatus ? _whiteKeyImageColor : _disabledKeyImageColor;
            RedKeyImage.color = redKeyStatus ? _redKeyImageColor : _disabledKeyImageColor;
            GreenKeyImage.color = greenKeyStatus ? _greenKeyImageColor : _disabledKeyImageColor;
            BlueKeyImage.color = blueKeyStatus ? _blueKeyImageColor : _disabledKeyImageColor;

            TargetPanelUI.SetActive(hasTarget);
        }

        private float _inventoryUIExitTime = 1.5f;
        private Coroutine _showInventoryUICoroutine;

        public void ShowInventoryUI()
        {
            InventoryUIGroup.SetActive(true);
            if (_showInventoryUICoroutine != null)
            {
                StopCoroutine(_showInventoryUICoroutine);
            }

            _showInventoryUICoroutine = StartCoroutine(InventoryExitDelay());
        }

        private IEnumerator InventoryExitDelay()
        {
            yield return new WaitForSeconds(_inventoryUIExitTime);
            InventoryUIGroup.SetActive(false);
        }

    }
}
