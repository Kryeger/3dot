﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts {
    public enum Speed {
        Running,
        Walking,
        Stopped,
        PlayerRunning,
        PlayerWalking,
        PlayerCrouching,
        PlayerStopped
    }

    public enum RotationSpeed {
        Normal = 10,
        Slow = 20,
    }

    public enum Tolerance {
        Standard = 10,
        CloseRange = 20
    }

    public static class Utils {
        private static readonly Dictionary<Tolerance, float> Tolerance = new Dictionary<Tolerance, float> {
            {Scripts.Tolerance.Standard, .00001f},
            {Scripts.Tolerance.CloseRange, 1f}
        };

        private static readonly Dictionary<Speed, float> Speed = new Dictionary<Speed, float> {
            {Scripts.Speed.Running, 8f},
            {Scripts.Speed.Walking, 5f},
            {Scripts.Speed.PlayerRunning, 12f},
            {Scripts.Speed.PlayerWalking, 8f},
            {Scripts.Speed.PlayerCrouching, 3f},
        };

        private static readonly Dictionary<RotationSpeed, float> RotationSpeed = new Dictionary<RotationSpeed, float> {
            {Scripts.RotationSpeed.Normal, 10f},
            {Scripts.RotationSpeed.Slow, 6f},
        };

        public static float GetSpeed(Speed type) {
            return Speed[type];
        }

        public static float GetRotationSpeed(RotationSpeed type) => RotationSpeed[type];

        public static float GetTolerance(Tolerance type) => Tolerance[type];


        public static bool IsSame(Vector3 a, Vector3 b) {
            return Vector3.Distance(a, b) < GetTolerance(Scripts.Tolerance.Standard);
        }

        public static bool IsSame(float a, float b) {
            return Math.Abs(a - b) < GetTolerance(Scripts.Tolerance.Standard);
        }

        public static bool IsClose(Vector3 a, Vector3 b) {
            return Vector3.Distance(a, b) < GetTolerance(Scripts.Tolerance.CloseRange);
        }

        public static bool IsClose(float a, float b) {
            return Math.Abs(a - b) < GetTolerance(Scripts.Tolerance.CloseRange);
        }

        public static GameObject GetChildByName(GameObject obj, string name) {
            var transform = obj.transform;
            var childTransform = transform.Find(name);
            return childTransform != null ? childTransform.gameObject : null;
        }

        public static bool IsInRangeExclusive(int test, int min, int max) {
            return (min < test && test < max);
        }

        public static bool IsInRangeInclusive(int test, int min, int max) {
            return (min <= test && test <= max);
        }
    }
}