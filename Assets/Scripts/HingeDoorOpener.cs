﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HingeDoorOpener : MonoBehaviour
{
    public float openAngle = 125f;
    public float closeAngle = 1f;
    public bool isSelfClosing = true;
    public float selfCloseAfterSeconds = 2f;

    private float _targetAngle = 1f;

    public GameObject door;
    private HingeJoint _doorHj;

    private bool _isOpen;

    private Coroutine _selfCloseCoroutine;

    private bool _isPlayerInReach;
    // Start is called before the first frame update
    void Start()
    {
        _doorHj = door.GetComponent<HingeJoint>();

        var limits = _doorHj.limits;
        limits.max = openAngle;
        _doorHj.limits = limits;
    }

    private IEnumerator SelfClose()
    {
        yield return new WaitForSeconds(selfCloseAfterSeconds);
        Toggle();
    }

    public void Toggle() {

        if (_isOpen)
        {
            if (_selfCloseCoroutine != null) StopCoroutine(_selfCloseCoroutine);
            _isOpen = false;
            _targetAngle = closeAngle;
        }
        else
        {
            _isOpen = true;
            _targetAngle = openAngle;
            if (isSelfClosing) _selfCloseCoroutine = StartCoroutine(SelfClose());
        }

        var spring = _doorHj.spring;

        spring.targetPosition = _targetAngle;

        _doorHj.spring = spring;

    }

    // Update is called once per frame
    void Update()
    {
        if (_isPlayerInReach && Input.GetKeyDown(KeyCode.E))
        {
            Toggle();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _isPlayerInReach = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _isPlayerInReach = false;
        }
    }
}
