﻿using UnityEngine;

namespace Scripts {
    public static class SceneComm {

        public static Save Save;

        public static Save StartSave = JsonUtility.FromJson<Save>(
            "{\"Position\":{\"x\":25.38172149658203,\"y\":2.500000238418579,\"z\":-80.26823425292969},\"Inventory\":{\"redKeyStatus\":false,\"greenKeyStatus\":false,\"whiteKeyStatus\":false,\"blueKeyStatus\":false,\"hasTarget\":false,\"saveCount\":0,\"bombCount\":3},\"DeathCount\":0,\"alarmStatus\":false,\"alarmDisabled\":false}");
        public static bool ShouldLoad = false;

    }
}