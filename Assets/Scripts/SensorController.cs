﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    public class SensorController : MonoBehaviour
    {
        public float startingSensorRange = 8f;
        private float _sensorRange = 8f;

        public float sensorRange {
            get => _sensorRange * (Mathf.Clamp(sensorLimitPct, 10, 100) / 100);
            set => _sensorRange = value;
        }

        [Range(0, 100)]
        public float sensorLimitPct;
        [Range(0, 361)] public float focusAngle;

        [Min(0)] public float findTargetsDelay = .2f;

        public LayerMask targetMask;
        public List<LayerMask> obstacleMasks = new List<LayerMask>();
        private LayerMask _obstacleMask;

        [HideInInspector] public List<Vector3> targetsInFocus = new List<Vector3>();


        public int targetsInFocusCount;

        public float meshResolution;
        public float edgeDistanceThreshold;
        public int edgeResolveIterations;

        public MeshFilter sensorMeshFilter = new MeshFilter();
        Mesh _sensorMesh;

        public GameObject player;

        public struct SensorCastInfo {
            public bool Hit;
            public Vector3 Point;
            public float Distance;
            public float Angle;
        }

        public struct EdgeInfo {
            public Vector3 PointA;
            public Vector3 PointB;
        }

        private void Awake()
        {
            player = GameObject.FindWithTag("Player");
        }

        private void Start() {
            _sensorMesh = new Mesh {name = "View Mesh"};
            sensorMeshFilter.mesh = _sensorMesh;

            sensorRange = startingSensorRange;
            sensorLimitPct = 100f;

            int obstacles = obstacleMasks[0];

            for (var i = 1; i < obstacleMasks.Count; ++i) {
                obstacles |= obstacleMasks[i];
            }

            _obstacleMask = obstacles;

            StartCoroutine(FindTargetsWithDelay(findTargetsDelay));
        }

        private void LateUpdate() {
            if (Vector3.Distance(player.transform.position, transform.position) <= 24)
            {
                DrawSensor();
            }
        }

        private IEnumerator FindTargetsWithDelay(float delay) {
            while (true) {
                yield return new WaitForSeconds(delay);
                FindTargetsInFocus();
            }

            // ReSharper disable once IteratorNeverReturns
        }

        private void OnDrawGizmosSelected() {
            Gizmos.color = new Color(1, 1, 1, .2f);
            Gizmos.DrawSphere(transform.position + GetPositionOffset(), sensorRange);
        }

        private void FindTargetsInFocus() {
            targetsInFocus.Clear();

            // TODO: optimize this with NonAlloc
            // ReSharper disable once Unity.PreferNonAllocApi
            var targetsInRange = Physics.OverlapSphere(transform.position + GetPositionOffset(), sensorRange, targetMask);

            foreach (var target in targetsInRange) {
                var targetTransform = target.transform;
                var dirToTarget = ((targetTransform.position + GetTargetPositionOffset()) - (transform.position + GetPositionOffset())).normalized;

                if (!(Vector3.Angle(transform.forward, dirToTarget) < (focusAngle / 2))) continue;

                var distanceToTarget = Vector3.Distance(transform.position + GetPositionOffset(), targetTransform.position + GetTargetPositionOffset());

                if (!Physics.Raycast(transform.position + GetPositionOffset(), dirToTarget, distanceToTarget, _obstacleMask))
                {
                    targetsInFocus.Add(target.transform.position);
                }
            }

            targetsInFocusCount = targetsInFocus.Count;
        }

        private void DrawSensor() {
            // TODO: huh?
            var stepCount = Mathf.RoundToInt(focusAngle * meshResolution);
            var stepAngleSize = focusAngle / stepCount;

            var sensorPoints = new List<Vector3>();
            var oldSensorCast = new SensorCastInfo();

            for (var i = 0; i < stepCount; i++) {
                var angle = transform.eulerAngles.y - focusAngle / 2 + stepAngleSize * i;
                var newSensorCast = SensorCast(angle);

                if (i > 0) {
                    var edgeDistanceThresholdExceeded =
                        Mathf.Abs(oldSensorCast.Distance - newSensorCast.Distance) > edgeDistanceThreshold;

                    if (oldSensorCast.Hit != newSensorCast.Hit ||
                        (oldSensorCast.Hit && edgeDistanceThresholdExceeded)) {
                        var edge = FindEdge(oldSensorCast, newSensorCast);
                        if (edge.PointA != Vector3.zero) {
                            sensorPoints.Add(edge.PointA);
                        }

                        if (edge.PointB != Vector3.zero) {
                            sensorPoints.Add(edge.PointB);
                        }
                    }
                }

                sensorPoints.Add(newSensorCast.Point);
                oldSensorCast = newSensorCast;
            }

            var vertexCount = sensorPoints.Count + 1;
            var vertices = new Vector3[vertexCount];
            var triangles = new int[(vertexCount - 2) * 3];

            vertices[0] = Vector3.zero;

            for (var i = 0; i < vertexCount - 1; i++) {
                vertices[i + 1] = transform.InverseTransformPoint(sensorPoints[i]);

                if (i < vertexCount - 2) {
                    triangles[i * 3] = 0;
                    triangles[i * 3 + 1] = i + 1;
                    triangles[i * 3 + 2] = i + 2;
                }
            }

            _sensorMesh.Clear();

            _sensorMesh.vertices = vertices;
            _sensorMesh.triangles = triangles;
            _sensorMesh.RecalculateNormals();
        }

        EdgeInfo FindEdge(SensorCastInfo minSensorCast, SensorCastInfo maxSensorCast) {
            var minAngle = minSensorCast.Angle;
            var maxAngle = maxSensorCast.Angle;
            var minPoint = Vector3.zero;
            var maxPoint = Vector3.zero;

            for (var i = 0; i < edgeResolveIterations; i++) {
                var angle = (minAngle + maxAngle) / 2;
                var newSensorCast = SensorCast(angle);

                var edgeDistanceThresholdExceeded =
                    Mathf.Abs(minSensorCast.Distance - newSensorCast.Distance) > edgeDistanceThreshold;

                if (newSensorCast.Hit == minSensorCast.Hit && !edgeDistanceThresholdExceeded) {
                    minAngle = angle;
                    minPoint = newSensorCast.Point;
                }
                else {
                    maxAngle = angle;
                    maxPoint = newSensorCast.Point;
                }
            }

            return new EdgeInfo {PointA = minPoint, PointB = maxPoint};
        }

        SensorCastInfo SensorCast(float globalAngle) {
            var direction = DirFromAngle(globalAngle, true);

            if (Physics.Raycast(transform.position + GetPositionOffset(), direction, out var hit, sensorRange, _obstacleMask)) {
                return new SensorCastInfo {Hit = true, Point = hit.point - GetPositionOffset(), Distance = hit.distance, Angle = globalAngle};
            }

            return new SensorCastInfo {
                Hit = false, Point = transform.position + direction * sensorRange, Distance = sensorRange,
                Angle = globalAngle
            };
        }

        public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal = false) {
            if (!angleIsGlobal) {
                angleInDegrees += transform.eulerAngles.y;
            }

            return new Vector3(
                Mathf.Sin(angleInDegrees * Mathf.Deg2Rad),
                0,
                Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
        }

        protected virtual Vector3 GetPositionOffset()
        {
            return Vector3.zero;
        }

        protected virtual Vector3 GetTargetPositionOffset()
        {
            return Vector3.zero;
        }
    }
}
