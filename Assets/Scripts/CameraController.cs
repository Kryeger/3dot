﻿using System.Collections;
using System.Collections.Generic;
using Scripts.Manager;
using UnityEngine;

namespace Scripts {
    public class CameraController : MonoBehaviour {
        public Transform player;
        public float smooth = .3f;
        public float height = 13f;
        public float range = 0.7f;
        private Vector3 _velocity = Vector3.zero;
        public Vector3 target;

        private float _crouchYmodifier = 10f;

        private Camera _mainCamera;
        public GameManager gameManager;

        private PlayerController _playerController;

        // Start is called before the first frame update
        private void Awake() {
            _mainCamera = Camera.main;
            var playerGO = GameObject.FindWithTag("Player");
            player = playerGO.GetComponent<Transform>();
            _playerController = playerGO.GetComponent<PlayerController>();
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        }

        // Update is called once per frame
        private void FixedUpdate() {
            if (Mathf.Abs(Input.GetAxis("Mouse ScrollWheel")) > 0.0001f && gameManager.godMode) // forward
            {
                height -= Input.GetAxis("Mouse ScrollWheel") * 10;
            }

            var ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
            const float hitDistance = 0f;
            var targetPoint = ray.GetPoint(hitDistance);

            var cameraPosition = transform.position;
            var mouseCorrection = (targetPoint - cameraPosition) * range;
            transform.position = Vector3.SmoothDamp(
                cameraPosition,
                (player.position + new Vector3(0, _playerController.isCrouching ? height - _crouchYmodifier : height, 0) + new Vector3(mouseCorrection.x, mouseCorrection.y, mouseCorrection.z * 1.77f)),
                ref _velocity, smooth
            );
        }
    }
}