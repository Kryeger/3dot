﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Manager {
    public class NoiseManager : MonoBehaviour
    {
        public GameObject noisePrefab;

        public Dictionary<int, NoiseController> noiseDict = new Dictionary<int, NoiseController>();

        public void MakeNoise(int instanceId, Vector3 position, float intensity) {
            if (!noiseDict.ContainsKey(instanceId)) {
                noiseDict[instanceId] = Instantiate(noisePrefab, position, Quaternion.identity).GetComponent<NoiseController>();
            }
            noiseDict[instanceId].SpawnAt(position, intensity);
        }

    }
}
