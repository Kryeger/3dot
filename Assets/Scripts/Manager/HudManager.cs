﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scripts.Manager {
    public class HudManager : MonoBehaviour {

        public GameObject pauseMenu;

        public Button pauseMenuContinue;
        public Button pauseMenuLoad;
        public Button exitMenuLoad;

        public PlayerController playerController;

        public bool isPaused = false;

        // Start is called before the first frame update
        void Start()
        {
            pauseMenu.SetActive(false);
            playerController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();

            pauseMenuContinue.onClick.AddListener(PauseMenuContinueClick);
            pauseMenuLoad.onClick.AddListener(PauseMenuLoadClick);
            exitMenuLoad.onClick.AddListener(PauseMenuExitClick);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                TogglePause();
            }
        }

        void TogglePause() {
            isPaused = !isPaused;
            if (isPaused) {
                Time.timeScale = 0;
            }
            else {
                Time.timeScale = 1;
            }

            playerController.inventoryController.ShowInventoryUI();
            playerController.postFxController.dof.active = isPaused;
            pauseMenu.SetActive(isPaused);
        }

        void PauseMenuContinueClick() {
            TogglePause();
        }

        void PauseMenuLoadClick() {
            playerController.saveController.Load();
            TogglePause();
        }

        void PauseMenuExitClick() {
            SceneManager.LoadScene("StartMenu");
        }
    }
}
