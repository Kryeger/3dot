using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Manager {
    public static class ResManagerEditor
    {
        public static Dictionary<ItemTypes, Material> KeyMaterials;
        public static Dictionary<ItemTypes, Material> DoorMaterials;

        public static void Init() {
            KeyMaterials = new Dictionary<ItemTypes, Material>{
                {ItemTypes.WhiteKey, Resources.Load<Material>("Materials/Keycard/WhiteKeycardMat")},
                {ItemTypes.RedKey, Resources.Load<Material>("Materials/Keycard/RedKeycardMat")},
                {ItemTypes.BlueKey, Resources.Load<Material>("Materials/Keycard/BlueKeycardMat")},
                {ItemTypes.GreenKey, Resources.Load<Material>("Materials/Keycard/GreenKeycardMat")}
            };
            DoorMaterials = new Dictionary<ItemTypes, Material>{
                {ItemTypes.WhiteKey, Resources.Load<Material>("Materials/Door/WhiteDoorMat")},
                {ItemTypes.RedKey, Resources.Load<Material>("Materials/Door/RedDoorMat")},
                {ItemTypes.BlueKey, Resources.Load<Material>("Materials/Door/BlueDoorMat")},
                {ItemTypes.GreenKey, Resources.Load<Material>("Materials/Door/GreenDoorMat")}
            };
        }

    }
}
