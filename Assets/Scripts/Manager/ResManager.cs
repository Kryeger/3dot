﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Manager {
    public class ResManager : MonoBehaviour
    {

        public Dictionary<ItemTypes, Material> KeyMaterials;
        public Dictionary<ItemTypes, Material> DoorMaterials;

        public Material whiteKeycardMat;
        public Material redKeycardMat;
        public Material blueKeycardMat;
        public Material greenKeycardMat;

        public Material whiteDoorMat;
        public Material redDoorMat;
        public Material blueDoorMat;
        public Material greenDoorMat;

        public Material HeadlightsOnMaterial;
        public Material StoplightsOnMaterial;

        public Material BlenderGrid;

        public void Init() {
            KeyMaterials = new Dictionary<ItemTypes, Material>{
                {ItemTypes.WhiteKey, whiteKeycardMat},
                {ItemTypes.RedKey, redKeycardMat},
                {ItemTypes.BlueKey, blueKeycardMat},
                {ItemTypes.GreenKey, greenKeycardMat}
            };
            DoorMaterials = new Dictionary<ItemTypes, Material>{
                {ItemTypes.WhiteKey, whiteDoorMat},
                {ItemTypes.RedKey, redDoorMat},
                {ItemTypes.BlueKey, blueDoorMat},
                {ItemTypes.GreenKey, greenDoorMat}
            };
        }

        void Awake()
        {
            Init();
        }
    }
}