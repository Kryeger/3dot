﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.Experimental.GlobalIllumination;
using UnityEngine.SceneManagement;

namespace Scripts.Manager {

    public class GameManager : MonoBehaviour {

        public float alarmLightMinIntensity = .3f;
        public float alarmLightMaxIntensity = 1;

        public UnityEvent alarmEvent;
        public UnityEvent stopAlarmEvent;

        public Light mainLight;
        public Color defaultLightColor;
        public float defaultLightIntensity;

        public Coroutine alarmLight;

        public GameObject MissionTitle;
        private float missionTitleScreenTime = 10f;

        public PlayerController playerController;

        public GameObject GoodMessage;
        public GameObject BadMessage;

        public bool endMessage = false;

        public bool godMode;

        private void Awake() {
            alarmEvent = new UnityEvent();
            alarmEvent.AddListener(StartAlarmLight);
            mainLight = GameObject.Find("Main Light").GetComponent<Light>();
            defaultLightColor = mainLight.color;
            defaultLightIntensity = mainLight.intensity;

            playerController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();

            stopAlarmEvent = new UnityEvent();
            stopAlarmEvent.AddListener(StopAlarmLight);


            Time.timeScale = 1;
            // Application.targetFrameRate = 60;
        }

        private void Start()
        {
            if (!SceneComm.ShouldLoad)
            {
                MissionTitle.SetActive(true);
                StartCoroutine(DisableMissionTitle());
            }
        }

        private IEnumerator DisableMissionTitle()
        {
            yield return new WaitForSeconds(missionTitleScreenTime);
            MissionTitle.SetActive(false);
        }

        private void StartAlarmLight() {
            mainLight.color = Color.red;
            alarmLight = StartCoroutine(AlarmLight());
        }

        private void StopAlarmLight() {
            mainLight.color = defaultLightColor;
            mainLight.intensity = defaultLightIntensity;
            if (alarmLight != null) {
                StopCoroutine(alarmLight);
            }
        }

        private IEnumerator AlarmLight() {
            while (true) {
                mainLight.intensity = alarmLightMaxIntensity;
                yield return new WaitForSeconds(.5f);
                mainLight.intensity = alarmLightMinIntensity;
                yield return new WaitForSeconds(.5f);
            }
        }

        private void SetGameMessage(GameObject gameMessage)
        {
            var position = playerController.transform.position;
            gameMessage.transform.position = new Vector3(position.x, gameMessage.transform.position.y, position.z);
            gameMessage.SetActive(true);
        }

        public void ShowEndScreen(bool goodEnding)
        {
            if (goodEnding)
            {
                SetGameMessage(GoodMessage);
                endMessage = true;
                StartCoroutine(GoToMainMenuTimeout());
            }
            else
            {
                SetGameMessage(BadMessage);
                endMessage = true;
                StartCoroutine(GoToMainMenuTimeout(true));
            }

        }

        private IEnumerator GoToMainMenuTimeout(bool restart = false)
        {
            yield return new WaitForSeconds(1.5f);
            if (restart) {
                SceneManager.LoadScene("scene-main");
            } else {

                SceneManager.LoadScene("StartMenu");
            }
        }

        public void PlayerGotCaught()
        {
            ShowEndScreen(false);
        }

    }
}
