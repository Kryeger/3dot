﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scripts.Manager {
    public class StartMenuController : MonoBehaviour
    {
        public Button startMenuPlay;
        public Button startMenuLoad;
        public Button startMenuQuit;

        public TextMeshPro loadBtnText;

        // Start is called before the first frame update
        void Start()
        {
            startMenuPlay.onClick.AddListener(StartMenuPlayClick);
            startMenuQuit.onClick.AddListener(StartMenuQuitClick);
            loadSave();
        }

        void loadSave() {
            try
            {
                SceneComm.Save =
                    JsonUtility.FromJson<Save>(
                        System.IO.File.ReadAllText(Application.persistentDataPath + "/save.json"));
            }
            catch (Exception e)
            {
                Debug.Log(e);
                SceneComm.Save = null;
            }

            if (SceneComm.Save != null) {
                startMenuLoad.onClick.AddListener(() => {
                    SceneComm.ShouldLoad = true;
                    SceneManager.LoadScene("scene-main");
                });
            } else
            {
                var loadText = startMenuLoad.GetComponentInChildren<TextMeshProUGUI>();
                var loadTextColor = loadText.color;
                loadText.color = new Color(loadTextColor.r, loadTextColor.g, loadTextColor.b, .2f);
            }
        }


        void StartMenuPlayClick()
        {
            SceneComm.ShouldLoad = false;
            SceneManager.LoadScene("scene-main");
        }

        void StartMenuQuitClick() {
            Application.Quit();
        }
    }
}
