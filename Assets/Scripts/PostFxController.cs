﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace Scripts {

    public class PostFxController : MonoBehaviour {

        public PostProcessVolume volume;
        public ChromaticAberration chromaticAberration;
        public DepthOfField dof;

        public float chrAbbIntensity {
            get => chromaticAberration.intensity.value;
            set {
                chromaticAberration.intensity.Interp(chromaticAberration.intensity, (Mathf.Clamp(value, 0, 1)), .01f);
            }
        }

        void Start() {
            volume = GetComponent<PostProcessVolume>();
            chromaticAberration = volume.profile.GetSetting<ChromaticAberration>();
            dof = volume.profile.GetSetting<DepthOfField>();
        }

    }
}
