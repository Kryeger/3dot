﻿using System;
using System.Collections;
using System.Collections.Generic;
using Scripts;
using Scripts.Manager;
using UnityEngine;

public class VehicleController : MonoBehaviour
{
    private GameObject _headlights;
    private GameObject _model;
    private Renderer _modelRenderer;

    private ResManager _resManager;

    public bool headlightsOn;
    public int headlightsMaterialIndex = -1;
    public int stoplightsMaterialIndex = -1;

    private void Awake()
    {
        _resManager = GameObject.Find("ResManager").GetComponent<ResManager>();

        _headlights = Utils.GetChildByName(gameObject, "Headlights");
        _model = Utils.GetChildByName(gameObject, "Model");
        _modelRenderer = _model.GetComponent<Renderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        SetHeadlights(headlightsOn);
    }

    private void SetHeadlights(bool state)
    {
        headlightsOn = state;
        _headlights.SetActive(state);
        var materials = _modelRenderer.materials;
        if (state)
        {
            if (headlightsMaterialIndex >= 0 && headlightsMaterialIndex < materials.Length)
            {
                materials[headlightsMaterialIndex] = _resManager.HeadlightsOnMaterial;
            }
            if (stoplightsMaterialIndex >= 0 && stoplightsMaterialIndex < materials.Length)
            {
                materials[stoplightsMaterialIndex] = _resManager.StoplightsOnMaterial;
            }
        }
        else
        {
            if (headlightsMaterialIndex >= 0 && headlightsMaterialIndex < materials.Length)
            {
                materials[headlightsMaterialIndex] = _resManager.BlenderGrid;
            }
            if (stoplightsMaterialIndex >= 0 && stoplightsMaterialIndex < materials.Length)
            {
                materials[stoplightsMaterialIndex] = _resManager.BlenderGrid;
            }
        }

        _modelRenderer.materials = materials;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
