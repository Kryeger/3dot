﻿using System.Collections;
using UnityEngine;

namespace Scripts {
    public class StaminaController : MonoBehaviour {
        [Range(0, 100)] private float _stamina = 100f;

        public float stamina => _stamina;

        private float _consumeRate = 1f;
        private float _regenRate = .9f;

        [Range(0, 100)] private float _startConsumeThreshold = 50f;

        private bool _isConsuming;
        private float _consumeStepDelay = .05f;
        private float _regenStepDelay = .05f;

        public bool StartConsuming() {
            if (_stamina >= _startConsumeThreshold) {
                _isConsuming = true;
            }

            return _isConsuming;
        }

        public void StopConsuming() {
            _isConsuming = false;
        }

        private void Start() {
            StartCoroutine(Cycle());
        }

        private IEnumerator Cycle() {
            while (true) {
                if (_isConsuming) {
                    yield return new WaitForSeconds(_consumeStepDelay);
                    Consume();

                    if (_stamina <= 0) {
                        _isConsuming = false;
                    }
                }
                else {
                    yield return new WaitForSeconds(_regenStepDelay);
                    Regen();
                }
            }

            // ReSharper disable once IteratorNeverReturns
        }

        private void Consume(float amount) {
            _stamina = Mathf.Clamp(_stamina - (_consumeRate * amount), 0, 100);
        }

        private void Regen(float amount) {
            _stamina = Mathf.Clamp(_stamina + (_regenRate * amount), 0, 100);
        }

        private void Consume() {
            Consume(_consumeRate);
        }

        private void Regen() {
            Regen(_regenRate);
        }
    }
}