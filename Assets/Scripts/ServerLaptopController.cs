﻿using System;
using System.Collections;
using System.Collections.Generic;
using Scripts;
using UnityEngine;

public class ServerLaptopController : MonoBehaviour
{
    public GameObject laptopScreen;

    public GameObject laptopLight;

    private float _downloadStepDelay = .3f;
    private float _textRemoveDelay = 10f;

    private Coroutine _downloadCoroutine;
    private Coroutine _downloadStatusCoroutine;

    private bool _downloadHasStarted;
    private bool _downloadIsActive;
    private float _downloadStep = .01f;

    private GameObject _player;

    private const string IN_PROGRESS_STATUS = "Download In Progress";
    private const string COMPLETE_STATUS = "Download Is Finished";
    private const string CONNECTION_LOST_STATUS = "Connection Lost";

    public GameObject hintText;

    public bool downloadIsActive
    {
        get => _downloadIsActive;
        set
        {
            _downloadIsActive = value;
            if (value)
            {
                StartDownloadCoroutine();
                downloadStatusTM.color = Color.green;
                downloadPercentageTM.color = Color.green;
                downloadStatusTM.text = IN_PROGRESS_STATUS;
            }
            else
            {
                StopDownloadCoroutine();
                downloadStatusTM.color = Color.red;
                downloadPercentageTM.color = Color.red;
                downloadStatusTM.text = CONNECTION_LOST_STATUS;
            }
        }
    }

    public ServerRoomFloorController serverRoomFloorController;

    private float _downloadPercentage = 0f;

    public TextMesh downloadStatusTM;
    public TextMesh downloadPercentageTM;

    public float DownloadPercentage
    {
        get => _downloadPercentage;
        set
        {if (value >= _downloadFullPercentage)
            {
                downloadStatusTM.color = Color.green;
                downloadPercentageTM.color = Color.green;
                downloadStatusTM.text = COMPLETE_STATUS;
                downloadPercentageTM.text = $"{_downloadFullPercentage:P2}";
                StartCoroutine(RemoveText());
                serverRoomFloorController.StopDownload();
                _player.GetComponent<InventoryController>().hasTarget = true;
            }
            else
            {
                downloadPercentageTM.text = $"{value:P2}";
            }

            _downloadPercentage = value;
        }
    }

    private float _downloadFullPercentage = 1f;
    // Start is called before the first frame update
    void Start()
    {
        laptopLight.SetActive(false);
        laptopScreen.SetActive(false);

        downloadStatusTM.gameObject.SetActive(false);
        downloadPercentageTM.gameObject.SetActive(false);

        downloadStatusTM.text = IN_PROGRESS_STATUS;
        downloadPercentageTM.text = $"{0:P2}";

        _player = GameObject.Find("Player");

    }

    private IEnumerator DownloadCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(_downloadStepDelay);
            DownloadPercentage += _downloadStep;
        }
    }

    public void StartDownloadCoroutine()
    {
        if (_downloadHasStarted && !IsDownloadFinished() && downloadIsActive)
        {
            if (_downloadCoroutine != null)
            {
                StopCoroutine(_downloadCoroutine);
            }
            _downloadCoroutine = StartCoroutine(DownloadCoroutine());
        }
    }

    public void StopDownloadCoroutine()
    {
        if (_downloadCoroutine != null)
        {
            StopCoroutine(_downloadCoroutine);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !_downloadHasStarted)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                hintText.SetActive(false);
                StartDownload();
            }
        }
    }

    private void StartDownload()
    {
        _downloadHasStarted = true;
        laptopLight.SetActive(true);
        laptopScreen.SetActive(true);

        downloadStatusTM.gameObject.SetActive(true);
        downloadPercentageTM.gameObject.SetActive(true);

        serverRoomFloorController.StartDownload();
    }

    public bool IsDownloadFinished()
    {
        //TODO: optimize this
        return _player.GetComponent<InventoryController>().hasTarget;
    }

    private IEnumerator RemoveText()
    {
        yield return new WaitForSeconds(_textRemoveDelay);
        laptopLight.SetActive(false);
        laptopScreen.SetActive(false);

        downloadStatusTM.gameObject.SetActive(false);
        downloadPercentageTM.gameObject.SetActive(false);
    }
}
