﻿using System;
using System.Collections.Generic;
using Scripts.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts {
    public class DoorController : MonoBehaviour {

        public GameObject model;
        public GameObject frame;

        public Renderer modelRenderer;
        public Renderer frameRenderer;


        public ItemTypes targetKey = ItemTypes.WhiteKey;
        public bool isLocked = false;

        private PlayerController _playerController;

        private ResManager _resManager;

        private void Awake() {
            _resManager = GameObject.Find("ResManager").GetComponent<ResManager>();
        }

        void Start() {
            UpdateMaterial();
            _playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        }

        #if UNITY_EDITOR
        private void OnValidate() {
            if ( modelRenderer == null || frameRenderer == null || ResManagerEditor.KeyMaterials[targetKey] == null) return;
            modelRenderer.material = ResManagerEditor.DoorMaterials[targetKey];
            frameRenderer.material = ResManagerEditor.DoorMaterials[targetKey];
        }
        #endif

        private void OnTriggerStay(Collider other) {
            if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Item")) {

                var playerInventory = _playerController.inventoryController;
                var locked = true;

                switch(targetKey){
                    case ItemTypes.RedKey:
                        locked = !playerInventory.redKeyStatus;
                        break;
                    case ItemTypes.GreenKey:
                        locked = !playerInventory.greenKeyStatus;
                        break;
                    case ItemTypes.BlueKey:
                        locked = !playerInventory.blueKeyStatus;
                        break;
                    case ItemTypes.WhiteKey:
                        locked = !playerInventory.whiteKeyStatus;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (playerInventory.hasTarget) isLocked = false;

                model.GetComponent<Rigidbody>().isKinematic = locked || isLocked;
            }
        }

        private void OnTriggerExit(Collider other) {
            if (other.gameObject.CompareTag("Player")) {
                model.GetComponent<Rigidbody>().isKinematic = false;
            }
        }

        void Update()
        {

        }

        void UpdateMaterial() {
            if ( modelRenderer == null || frameRenderer == null || _resManager.KeyMaterials[targetKey] == null) return;
            modelRenderer.material = _resManager.DoorMaterials[targetKey];
            frameRenderer.material = _resManager.DoorMaterials[targetKey];
        }
    }
}
