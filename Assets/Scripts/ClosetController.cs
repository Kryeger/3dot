﻿using System;
using System.Collections;
using UnityEngine;

namespace Scripts {
    public class ClosetController : MonoBehaviour {
        public float openAngle = 125f;
        public bool isSelfClosing = true;
        public float selfCloseAfterSeconds = 2f;

        private float _targetAngle = 1f;

        public GameObject doorL;
        public GameObject doorR;

        public Rigidbody rbL;
        public Rigidbody rbR;

        public GameObject boxTop;

        private HingeJoint hjL;
        private HingeJoint hjR;

        private bool _isOpen;

        private Coroutine _selfCloseCoroutine;

        private bool _isPlayerInReach;

        // Start is called before the first frame update
        void Start() {
            rbL = doorL.GetComponent<Rigidbody>();
            rbR = doorR.GetComponent<Rigidbody>();

            hjL = doorL.GetComponent<HingeJoint>();
            hjR = doorR.GetComponent<HingeJoint>();

            var limits = hjL.limits;
            limits.max = openAngle;
            hjL.limits = limits;
            hjR.limits = limits;
        }

        private IEnumerator SelfClose()
        {
            yield return new WaitForSeconds(selfCloseAfterSeconds);
            Toggle();
        }

        public void Toggle() {

            if (_isOpen)
            {
                if (_selfCloseCoroutine != null) StopCoroutine(_selfCloseCoroutine);
                _isOpen = false;
                _targetAngle = 1;
                boxTop.SetActive(true);
            }
            else
            {
                _isOpen = true;
                _targetAngle = openAngle;
                boxTop.SetActive(false);
                if (isSelfClosing) _selfCloseCoroutine = StartCoroutine(SelfClose());
            }

            var spring = hjL.spring;

            spring.targetPosition = _targetAngle;

            hjL.spring = spring;
            hjR.spring = spring;

        }



        public bool ReachedMaxLimits() {
            var result = Utils.IsClose(rbR.GetComponent<HingeJoint>().angle, rbR.GetComponent<HingeJoint>().limits.max)
                         && Utils.IsClose(rbL.GetComponent<HingeJoint>().angle,
                             rbL.GetComponent<HingeJoint>().limits.max);
            Debug.Log("Reached max limits: " + result);
            return result;
        }

        public bool ReachedMinLimits() {
            var result = Utils.IsClose(rbR.GetComponent<HingeJoint>().angle, rbR.GetComponent<HingeJoint>().limits.min)
                         && Utils.IsClose(rbL.GetComponent<HingeJoint>().angle,
                             rbL.GetComponent<HingeJoint>().limits.min);
            Debug.Log("Reached min limits: " + result);
            return result;
        }

        private void Update()
        {
            if (_isPlayerInReach && Input.GetKeyDown(KeyCode.E))
            {
                Toggle();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _isPlayerInReach = true;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                _isPlayerInReach = false;
            }
        }
    }
}
