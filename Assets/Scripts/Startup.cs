﻿using Scripts.Manager;
using UnityEditor;

#if UNITY_EDITOR

namespace Scripts {
    [InitializeOnLoad]
    public class Startup
    {
        static Startup () {
            ResManagerEditor.Init();
        }
    }
}

#endif

