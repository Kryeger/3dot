﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Scripts {
    public enum PatrolType {
        Loop,
        BackAndForth
    }

    public class PatrolController : MonoBehaviour {
        public List<GameObject> points = new List<GameObject>();

        private int _currentTargetIndex = 0;

        public PatrolType type = PatrolType.Loop;

        private int _backAndForthModifier = 1;

        public int currentTargetIndex {
            get => _currentTargetIndex;
            set {
                if (value >= 0 && value < points.Count) {
                    _currentTargetIndex = value;
                }
            }
        }

        public GameObject currentTarget
        {
            get
            {
                if (currentTargetIndex < points.Count)
                {
                    return points[currentTargetIndex];
                }

                return null;
            }
        }

        private void Start() {
            foreach (Transform child in transform) {
                points.Add(child.gameObject);
            }
        }

        public void AdvanceTargetIndex() {
            switch (type) {
                case PatrolType.Loop:
                    currentTargetIndex = (currentTargetIndex + 1) % points.Count;
                    break;
                case PatrolType.BackAndForth:
                    var nextIndex = currentTargetIndex + _backAndForthModifier;

                    if (nextIndex >= points.Count) {
                        _backAndForthModifier = -1;
                        currentTargetIndex -= 1;
                    }
                    else if (nextIndex < 0) {
                        _backAndForthModifier = 1;
                        currentTargetIndex += 1;
                    }
                    else {
                        currentTargetIndex = nextIndex;
                    }

                    break;
            }
        }

        public int FindClosestPointIndex(Vector3 position) =>
            points.Count == 0 ?
                0 :
                points
                    .Select((value, index) => new {Index = index, Value = value})
                    .Aggregate((a, b) =>
                        Vector3.Distance(position, a.Value.transform.position) <
                        Vector3.Distance(position, b.Value.transform.position)
                            ? a
                            : b)
                    .Index;

        public void AdvanceToClosestPointIndex(Vector3 position) {
            currentTargetIndex = FindClosestPointIndex(position);
        }
    }
}