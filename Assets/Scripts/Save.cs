﻿using System;
using UnityEngine;

namespace Scripts {

    [Serializable]
    public struct InventorySave {
        public bool redKeyStatus, greenKeyStatus, whiteKeyStatus, blueKeyStatus, hasTarget;
        public int saveCount, bombCount;

        public InventorySave(InventoryController inventoryController) {
            saveCount = inventoryController.saveCount;
            redKeyStatus = inventoryController.redKeyStatus;
            greenKeyStatus = inventoryController.greenKeyStatus;
            whiteKeyStatus = inventoryController.whiteKeyStatus;
            blueKeyStatus = inventoryController.blueKeyStatus;
            hasTarget = inventoryController.hasTarget;
            bombCount = inventoryController.bombCount;
        }
    }

    public class Save {

        public Save(PlayerController playerController, AlarmController alarmController) {
            Position = playerController.transform.position;
            Inventory = new InventorySave(playerController.inventoryController);
            DeathCount = playerController.deathCount;
            alarmStatus = alarmController.alarmStatus;
            alarmDisabled = alarmController.disabled;
        }

        public Vector3 Position;
        public InventorySave Inventory;
        public int DeathCount;
        public bool alarmStatus, alarmDisabled;

    }
}